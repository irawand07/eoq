<?php
  include_once "_template_atas.php";
?>

<?php

if(isset($_GET['cari'])){
  $tglAwalLabel = $_GET['tgl_awal'];
  $tglAkhirLabel = $_GET['tgl_akhir'];
  $arrTglAwal = explode('-',$tglAwalLabel);
  $arrTglAkhir = explode('-',$tglAkhirLabel);
  $tglAwal = $arrTglAwal[2]."-".$arrTglAwal[1]."-".$arrTglAwal[0];
  $tglAkhir = $arrTglAkhir[2]."-".$arrTglAkhir[1]."-".$arrTglAkhir[0];
}else{
  $tglAwal = date('Y-m-d');
  $tglAkhir = date('Y-m-d');
  $tglAwalLabel = date('d-m-Y');
  $tglAkhirLabel =date('d-m-Y');
}

  $dataPembelian = $conn->query("
    SELECT
      pembelian.`id_beli`,
      pembelian.`no_pembelian`,
      pembelian.`total`,
      pembelian.`keterangan`,
      DATE_FORMAT(pembelian.`tanggal_beli`, '%d-%m-%Y %H:%i') AS tgl,
      supplier.`nama_supplier`,
      karyawan.`nama_karyawan`
    FROM pembelian
    LEFT JOIN supplier ON pembelian.`kd_supplier` = supplier.`kd_supplier`
    LEFT JOIN karyawan ON pembelian.`kd_karyawan` = karyawan.`kd_karyawan`
    WHERE DATE_FORMAT(tanggal_beli, '%Y-%m-%d') BETWEEN '$tglAwal' AND '$tglAkhir'
    ORDER BY tanggal_beli
  ");
?>

<script type="text/javascript" src="plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<link rel="stylesheet" href="plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css"/>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Laporan Pembelian</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
            <li class="breadcrumb-item ">Laporan</li>
            <li class="breadcrumb-item active">Pembelian</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">

      <div class="row">
        <div class="col-6">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Pencarian</h3>
            </div>
            <div class="card-body">
              <form method="get">
                <div class="form-group">
                  <label for="exampleInputEmail1">Tanggal Awal</label>
                  <input type="text" name="tgl_awal" class="form-control" placeholder="Tanggal Awal">
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Tanggal Akhir</label>
                  <input type="text" name="tgl_akhir" class="form-control" placeholder="Tanggal Akhir">
                </div>
                <button type="submit" name="cari" class="btn btn-primary">Cari</button>
              </form>
            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Laporan Pembelian Tanggal <?= $tglAwalLabel ?> sampai <?= $tglAkhirLabel ?> </h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">

              <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th width="50px">No</th>
                  <th>No Pembelian</th>
                  <th>Tanggal Pembelian</th>
                  <th>Nama Supplier</th>
                  <th>Keterangan</th>
                  <th>Karyawan</th>
                  <th>Total Pembelian (Rp)</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                  <?php
                    if(!empty($dataPembelian)){
                      $no = 1;
                      foreach($dataPembelian as $row){
                        echo "<tr>";
                        echo "<td>".$no."</td>";
                        echo "<td>".$row['no_pembelian']."</td>";
                        echo "<td>".$row['tgl']."</td>";
                        echo "<td>".$row['nama_supplier']."</td>";
                        echo "<td>".$row['keterangan']."</td>";
                        echo "<td>".$row['nama_karyawan']."</td>";
                        echo "<td align='right'>".number_format($row['total'],2,',','.')."</td>";
                        echo "<td align='center'><a class='btn btn-xs btn-primary' href='lap_pembelian_detail.php?id=".$row['id_beli']."'>Detail</a></td>";
                        echo "</tr>";
                        $no++;
                      }
                    }
                  ?>
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
        </div>
      </div>

    </div>
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<?php
  include_once "_template_bawah.php";
?>

<script>
// $('.datepicker').datepicker({
//    format: 'dd/mm/yyyy'
// });
var date_input=$('input[name="tgl_awal"]'); //our date input has the name "date"
var date_input2=$('input[name="tgl_akhir"]'); //our date input has the name "date"
var options={
  dateFormat: 'dd-mm-yy',
  todayHighlight: true,
  autoclose: true,
};
date_input.datepicker(options);
date_input2.datepicker(options);

$('input[name="tgl_awal"]').datepicker("setDate", '<?= $tglAwalLabel ?>' );
$('input[name="tgl_akhir"]').datepicker("setDate", '<?= $tglAkhirLabel ?>' );
</script>
