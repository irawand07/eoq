<?php
error_reporting(0);
ini_set('display_errors', '0');

// koneksi ke database
include 'koneksi.php';
date_default_timezone_set('Asia/Jakarta');

function query($query) {
	global $conn;
	$result = mysqli_query($conn, $query);
	$rows = [];
	while ( $row = mysqli_fetch_assoc($result) ) {
		$rows[] = $row;
	}
	return $rows;
}

?>
