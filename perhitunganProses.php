<?php
  include_once "_template_atas.php";
?>

<?php
  $bahanbaku = $_POST['bahanbaku'];
  $harga = $_POST['harga'];
  $biaya_pesan = $_POST['biaya_pesan'];
  $biaya_simpan = $_POST['biaya_simpan'];
  $leadtime = $_POST['leadtime'];
  $kebutuhan = $_POST['kebutuhan'];
  $hari_kerja = $_POST['hari_kerja'];

  $getBahanbaku = query("SELECT nama_bahanbaku FROM bahan_baku WHERE kd_bahanbaku = '$bahanbaku' ")[0];

  $arrResult = array(
    "bahanbaku" => $bahanbaku,
    "nama" => $getBahanbaku['nama_bahanbaku'],
    "harga" => $harga,
    "biaya_pesan" => $biaya_pesan,
    "biaya_simpan" => $biaya_simpan,
    "leadtime" => $leadtime,
    "kebutuhan" => $kebutuhan,
    "hari_kerja" => $hari_kerja
  );

  //mulai perhitungan eoq,ss,rop
  $kebutuhan_pertahun = $kebutuhan * $hari_kerja;
  $kebutuhan_max_pm = $kebutuhan * 6; //kebutuhan max perminggu
  $kebutuhan_rata_pm = $kebutuhan_pertahun / 52; //kebutuhan rata2 perminggu

  $eoq = sqrt(2 * ($kebutuhan_pertahun * $biaya_pesan) / ($harga * ($biaya_simpan/100) ) );
  $ss = ( $kebutuhan_max_pm - $kebutuhan_rata_pm ) * $leadtime;
  $rop = ($leadtime * $kebutuhan_rata_pm) + $ss;
  $frekuensi = $kebutuhan_pertahun / $eoq;
  // selesai

  //simpan ke session
  $arrResult['kebutuhan_pertahun'] = $kebutuhan_pertahun;
  $arrResult['eoq'] = round($eoq,2);
  $arrResult['ss'] = round($ss);
  $arrResult['rop'] = round($rop);
  $arrResult['frekuensi'] = round($frekuensi);

  $_SESSION['perhitungan'][$bahanbaku] = $arrResult;

  echo "<script>document.location.href = 'perhitunganTambah.php';</script>";
  die();

?>

<?php
  include_once "_template_bawah.php";
?>
