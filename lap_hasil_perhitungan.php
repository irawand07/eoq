<?php
  include_once "_template_atas.php";
?>

<?php
  $dataBahanbaku = $conn->query("SELECT * FROM bahan_baku ORDER BY kd_bahanbaku ");
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Laporan Hasil Perhitungan EOQ, SS dan ROP</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
            <li class="breadcrumb-item">Laporan</li>
            <li class="breadcrumb-item active">Hasil Perhitungan</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">

      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Laporan Hasil Perhitungan EOQ, SS dan ROP Per Tanggal <?= date('d-m-Y') ?></h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">

              <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th width="50px">No</th>
                  <th>Nama</th>
                  <th>Tanggal</th>
                  <th>Harga</th>
                  <th>Biaya Pesan</th>
                  <th>Biaya Simpan</th>
                  <th>Leadtime</th>
                  <th>Kebutuhan per Tahun</th>
                  <th>EOQ</th>
                  <th>SS</th>
                  <th>ROP</th>
                  <th>Frek</th>
                </tr>
                </thead>
                <tbody>
                  <?php
                    if(!empty($dataBahanbaku)){
                      $no = 1;
                      foreach($dataBahanbaku as $row){
                        $kdBahanBaku = $row['kd_bahanbaku'];
                        $hslPerhitungan = query("
                          SELECT
                                DATE_FORMAT(perhitungan.`tanggal`, '%d-%m-%Y %H:%i') AS tgl,
                                harga_satuan,
                                biaya_pesan,
                                biaya_simpan,
                                kebutuhan_perhari,
                                kebutuhan_pertahun,
                                lead_time,
                                safety_stok,
                                eoq,
                                reorder_poin,
                                frekuensi
                          FROM perhitungan
                          JOIN detail_perhitungan ON  detail_perhitungan.`id_perhitungan` = perhitungan.`id_perhitungan`
                          WHERE detail_perhitungan.`kd_bahanbaku` = '$kdBahanBaku'
                          ORDER BY perhitungan.`tanggal` DESC
                          LIMIT 1
                        ")[0];

                        echo "<tr>";
                        echo "<td>".$no."</td>";
                        echo "<td>".$row['nama_bahanbaku']."</td>";

                        if(!empty($hslPerhitungan)){
                          echo "<td>".$hslPerhitungan['tgl']."</td>";
                          echo "<td>".number_format($hslPerhitungan['harga_satuan'],2,',','.')."</td>";
                          echo "<td>".number_format($hslPerhitungan['biaya_pesan'],2,',','.')."</td>";
                          echo "<td>".number_format($hslPerhitungan['biaya_simpan'],0,',','.')."</td>";
                          echo "<td>".number_format($hslPerhitungan['lead_time'],0,',','.')."</td>";
                          echo "<td>".number_format($hslPerhitungan['kebutuhan_pertahun'],0,',','.')."</td>";
                          echo "<td>".number_format($hslPerhitungan['eoq'],2,',','.')."</td>";
                          echo "<td>".number_format($hslPerhitungan['safety_stok'],2,',','.')."</td>";
                          echo "<td>".number_format($hslPerhitungan['reorder_poin'],2,',','.')."</td>";
                          echo "<td>".number_format($hslPerhitungan['frekuensi'],2,',','.')."</td>";
                        }else{
                          echo "<td></td>";
                          echo "<td></td>";
                          echo "<td></td>";
                          echo "<td></td>";
                          echo "<td></td>";
                          echo "<td></td>";
                          echo "<td></td>";
                          echo "<td></td>";
                          echo "<td></td>";
                          echo "<td></td>";
                        }

                        echo "</tr>";
                        $no++;
                      }
                    }
                  ?>
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
        </div>
      </div>

    </div>
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<?php
  include_once "_template_bawah.php";
?>
