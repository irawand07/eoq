<?php
  include_once "_template_atas.php";
?>

<?php

  $id = $_GET['id'];

  $dataPemakaian = query("
    SELECT
      pemakaian.`id_pemakaian`,
      pemakaian.`no_pemakaian`,
      pemakaian.`total`,
      pemakaian.`keterangan`,
      DATE_FORMAT(pemakaian.`tanggal`, '%d-%m-%Y %H:%i') AS tgl,
      karyawan.`nama_karyawan`
    FROM pemakaian
    LEFT JOIN karyawan ON pemakaian.`kd_karyawan` = karyawan.`kd_karyawan`
    WHERE id_pemakaian = '$id'
  ")[0];

  $detail = query("
    SELECT
      bahan_baku.kd_bahanbaku,
      bahan_baku.nama_bahanbaku,
      qty,
      harga,
      subtotal
    FROM detail_pemakaian
    LEFT JOIN bahan_baku ON bahan_baku.kd_bahanbaku = detail_pemakaian.kd_bahanbaku
    WHERE id_pemakaian = '$id'
  ");
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Laporan Pemakaian Detail</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
            <li class="breadcrumb-item ">Laporan</li>
            <li class="breadcrumb-item active">Pemakaian Detail</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">

      <div class="row">
        <div class="col-6">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Pemakaian</h3>
            </div>
            <div class="card-body">
              <table width="100%">
                <tr>
                  <td width="170px">No Pemakaian</td>
                  <td width="1%">: </td>
                  <td><?= $dataPemakaian['no_pemakaian'] ?></td>
                </tr>
                <tr>
                  <td>Tanggal Pemakaian</td>
                  <td>: </td>
                  <td><?= $dataPemakaian['tgl'] ?></td>
                </tr>
                <tr>
                  <td>Keterangan</td>
                  <td>: </td>
                  <td><?= $dataPemakaian['keterangan'] ?></td>
                </tr>
                <tr>
                  <td>Karyawan</td>
                  <td>: </td>
                  <td><?= $dataPemakaian['nama_karyawan'] ?></td>
                </tr>
              </table>
            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Detail </h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table class="table table-bordered" width="100%">
                <thead>
                  <th style="text-align:center">No</th>
                  <th style="text-align:center">Kode Bahan baku</th>
                  <th style="text-align:center">Nama Bahan baku</th>
                  <th style="text-align:center">Qty</th>
                  <th style="text-align:center">Harga Beli</th>
                  <th style="text-align:center">Subtotal</th>
                </thead>
                <tbody>
                <?php
                  if(!empty($detail)){
                    $no = 1;
                    foreach($detail as $row){
                      echo "<tr>";
                      echo "<td>".$no."</td>";
                      echo "<td>".$row['kd_bahanbaku']."</td>";
                      echo "<td>".$row['nama_bahanbaku']."</td>";
                      echo "<td align='right'>".$row['qty']."</td>";
                      echo "<td align='right'>".number_format($row['harga'],2,',','.')."</td>";
                      echo "<td align='right'>".number_format($row['subtotal'],2,',','.')."</td>";
                      echo "</tr>";
                      $no++;
                    }
                    echo "<td align='right' colspan='5'><b>Total Pemakaian</b></td>";
                    echo "<td align='right' colspan='5'><b>".number_format($dataPemakaian['total'],2,',','.')."</b></td>";
                  }
                ?>
                </tbody>
              </table>
              <br/>
              <a href="lap_pemakaian.php" class="btn btn-default">Kembali</a>
            </div>
            <!-- /.card-body -->
          </div>
        </div>
      </div>

    </div>
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<?php
  include_once "_template_bawah.php";
?>

<script>
// $('.datepicker').datepicker({
//    format: 'dd/mm/yyyy'
// });
var date_input=$('input[name="tgl_awal"]'); //our date input has the name "date"
var date_input2=$('input[name="tgl_akhir"]'); //our date input has the name "date"
var options={
  dateFormat: 'dd-mm-yy',
  todayHighlight: true,
  autoclose: true,
};
date_input.datepicker(options);
date_input2.datepicker(options);

$('input[name="tgl_awal"]').datepicker("setDate", '<?= $tglAwalLabel ?>' );
$('input[name="tgl_akhir"]').datepicker("setDate", '<?= $tglAkhirLabel ?>' );
</script>
