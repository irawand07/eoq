<?php
  include_once "_template_atas.php";
?>

<?php
$inv = $_POST["penjualan_invoice2"];
if( isset($_POST["addPemakaian"]) ){
  $check = true;
  if(empty($_POST['keranjang_qty']) || trim($_POST['no_invoice']) == '' || $_POST['keranjang_total'] < 0 ){
		$check = false;
	}
  $total = (float)$_POST['keranjang_total'];
	$noInvoice = htmlspecialchars($_POST['no_invoice']);
  $keterangan = htmlspecialchars($_POST['keterangan']);
	$keranjangQty = $_POST['keranjang_qty'];
	$userId = $_SESSION['u_kode'];

  if($check == true){

    $totalTrans = 0;
  	$arrDetail = array();
  	$i=0;
    $stokTersedia = true;
  	foreach($keranjangQty as $key => $value){

      $getBahanBaku = query("
  				SELECT
            *
  				FROM bahan_baku
  				WHERE kd_bahanbaku = '$key'
  		")[0];

  		$harga = $getBahanBaku['harga_satuan'];
  		$stok = $getBahanBaku['stok_akhir'];

  		$arrDetail[$i]['id'] = $key;
  		$arrDetail[$i]['harga'] = $harga;
  		$arrDetail[$i]['qty'] = $keranjangQty[$key];
  		$arrDetail[$i]['subtotal'] = $value * $harga;
  		$totalTrans = $totalTrans + $arrDetail[$i]['subtotal'];
      if($stok<$arrDetail[$i]['qty'] || empty($getBahanBaku) || $arrDetail[$i]['qty'] <= 0){
  			$stokTersedia = false;
  			break;
  		}
  		$i++;
  	}

  	if($stokTersedia === false || $total != $totalTrans){
  		$result = false;
  	}else{

      $conn->begin_transaction();
    	//insert ke pemakaian
    	$tglTrans = date('Y-m-d H:i:s');
    	$pema = $conn->query("
    			INSERT INTO `pemakaian`
    				(`no_pemakaian`, `tanggal`, `total`, `kd_karyawan`,`keterangan`)
    			VALUES
    				('$noInvoice', '$tglTrans', '$total', '$userId', '$keterangan')
    	");

      $idPema = query("SELECT id_pemakaian FROM pemakaian WHERE no_pemakaian = '$noInvoice' LIMIT 1 ")[0]['id_pemakaian'];
      //insert ke detail
    	$insertDet = true;
    	foreach($arrDetail as $row){
    		$rId = $row['id'];
    		$rQty = $row['qty'];
    		$rHrg = $row['harga'];
    		$rSub = $row['subtotal'];

    		$pemaDetail = $conn->query("
    				INSERT INTO `detail_pemakaian`
    					(`id_pemakaian`, `kd_bahanbaku`, `qty`, `harga`, `subtotal`)
    				VALUES
    				('$idPema', '$rId', '$rQty', '$rHrg', '$rSub')
    		");

        $getIdDetail= query("SELECT id_detpemakaian FROM detail_pemakaian WHERE kd_bahanbaku = '$rId' AND id_pemakaian ='$idPema' LIMIT 1 ")[0]['id_detpemakaian'];
        $getStok = query("SELECT stok_akhir, harga_satuan FROM bahan_baku WHERE kd_bahanbaku = '$rId' LIMIT 1 ")[0];

        $persQty = $getStok['stok_akhir'] - $rQty;
        $persTotal = ($getStok['stok_akhir'] * $getStok['harga_satuan']) - $rSub;
        $persHarga = $getStok['harga_satuan'];

        $insertPersediaan = $conn->query("
        INSERT INTO `persediaan`
          (`id_detpemakaian`, `kd_bahanbaku`, `tanggal`, `qty`, `harga`, `total`)
        VALUES
          ('$getIdDetail', '$rId', '$tglTrans', '$persQty', '$persHarga', '$persTotal');
    		");

        $updateStok = $conn->query("
    				UPDATE bahan_baku
            SET
              stok_akhir = '$persQty'
            WHERE
              kd_bahanbaku = '$rId'
    		");

    		$insertDet = $insertDet && $pemaDetail && $insertPersediaan && $updateStok;
    	}
    	$result = $insertDet && $pema;
      if(!$result){
    		$conn->rollback();
    	}else{
    		$conn->commit();
    	}
    }

  }

  if($result === true){
    $addStatus = '1';
  }else{
    $addStatus = '0';
  }

  $addInvoice = $noInvoice;
  echo "
    <script>
      document.location.href = 'pemakaian.php?status=".$addStatus."&no_invoice=".$addInvoice."';
    </script>
  ";
  exit;
}

if(isset($_GET["status"])){
  if($_GET["status"] == '1') {
    $pesan = 'sukses';
    $invoiceProses = $_GET['no_invoice'];
  }elseif($_GET["status"] == '0'){
    $pesan = 'gagal';
    $invoiceProses = $_GET['no_invoice'];
  }
}
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Transaksi Pemakaian</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
            <li class="breadcrumb-item active">Transaksi Pemakaian</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
  <?php

    $pemakaian = query("SELECT max(no_pemakaian) AS last FROM pemakaian ")[0];
    if($pemakaian['last'] == ''){
      $noAkhir = 'PKA000000';
    }else{
      $noAkhir = $pemakaian['last'];
    }
    $urutan = (int) substr($noAkhir, 3, 6);
    $urutan++;

    $huruf = "PKA";
    $noInvoice = $huruf . sprintf("%06s", $urutan);

    $data = query("
      SELECT
        *
      FROM bahan_baku
      ORDER BY kd_bahanbaku ASC
    ");
  ?>
      <div class="row">
      <div class="col-md-8">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Daftar Bahan baku</h3>
          </div>
          <div class="card-body">
              <?php foreach($data as $row){ ?>
                  <input type="hidden" id="bahanbaku_kode_<?= $row['kd_bahanbaku'] ?>" value="<?= $row['kd_bahanbaku'] ?>">
                  <input type="hidden" id="bahanbaku_nama_<?= $row['kd_bahanbaku'] ?>" value="<?= $row['nama_bahanbaku'] ?>">
                  <input type="hidden" id="bahanbaku_stok_<?= $row['kd_bahanbaku'] ?>" value="<?= $row['stok_akhir'] ?>">
                  <input type="hidden" id="bahanbaku_harga_<?= $row['kd_bahanbaku'] ?>" value="<?= $row['harga_satuan'] ?>">
              <?php } ?>
              <table id="example3" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th style="width: 6%;">No.</th>
                  <th style="width: 20%;">Kode Bahan baku</th>
                  <th>Nama Bahan baku</th>
                  <th>Harga (Rp)</th>
                  <th>Stok</th>
                  <th style="text-align: center;">Aksi</th>
                </tr>
                </thead>
                <tbody>
                  <?php $i=1; ?>
                  <?php foreach($data as $row) : ?>
                  <tr>
                      <td><?= $i; ?></td>
                      <td><?= $row['kd_bahanbaku'] ?></td>
                      <td><?= $row['nama_bahanbaku'] ?></td>
                      <td>
                        <?= number_format($row['harga_satuan'],2) ?>
                      </td>
                      <td>
                        <?= $row['stok_akhir'] ?>
                      </td>
                      <td style="text-align: center; width: 17%;">
                          <?php
                          if($row['stok_akhir'] > 0){
                            echo '
                            <button onclick="insertKeranjang(\''.$row['kd_bahanbaku'].'\')" class="btn btn-xs btn-primary" type="button" name="submit">
                            <i class="fa fa-shopping-cart"></i> Pilih
                            </button>
                            ';
                          }else{
                            echo '
                              <button disabled class="btn btn-xs btn-secondary" type="button">
                                <i class="fa fa-shopping-cart"></i> Pilih
                              </button>
                            ';
                          }
                          ?>
                      </td>
                  </tr>
                  <?php $i++; ?>
                  <?php endforeach; ?>
                </tbody>
              </table>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Informasi</h3>
          </div>
          <div class="card-body">
            <?php if($pesan == 'sukses'){ ?>
            <div class="alert alert-success alert-dismissible" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <strong><?= $invoiceProses ?></strong> Berhasil Disimpan
            </div>
            <?php }elseif($pesan == 'gagal'){ ?>
            <div class="alert alert-danger alert-dismissible" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <strong><?= $invoiceProses ?></strong> Gagal Disimpan
            </div>
            <?php } ?>
            <table>
              <tr>
                <td width="120px">No Pemakaian</td>
                <th>:
                  <?php
                    echo $noInvoice;
                  ?>
                </th>
              </tr>
              <tr>
                <td>User</td>
                <th>: <?= $_SESSION['u_nama'] ?></th>
              </tr>
              <tr>
                <td>Tanggal</td>
                <th>: <?= date("d-m-Y") ?></th>
              </tr>
            </table>
          </div>
        </div>
      </div>
      </div>
      <form method="post" onsubmit="return validateMyForm(event)" name="formPemakaian">
      <div class="row">
      <div class="col-lg-8">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Keranjang</h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <div class="table-auto">
              <table id="keranjang" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th width="10%">Kode</th>
                    <th>Nama</th>
                    <th width="20%">Harga Beli (Rp)</th>
                    <th width="10%" style="text-align: center;">QTY</th>
                    <th width="20%">Sub Total (Rp)</th>
                    <th width="5%" style="text-align: center;">Aksi</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
          </div>
          <!-- /.card-body -->
        </div>
      </div>
      <div class="col-lg-4">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Total Pemakaian</h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <table class="table" >
              <tr>
                  <td><b>Total</b></td>
                  <td class="table-nominal">
                     <span>Rp. </span>
                     <span>
                        <input type="hidden" name="no_invoice" value="<?= $noInvoice; ?>">
                        <input type="hidden" name="keranjang_total" id="keranjang_total" value="0">
                        <input type="text" name="invoice_total" id="keranjang_total_label" class="a2"  value="<?= $total; ?>" size="10" readonly>
                     </span>
                  </td>
              </tr>
              <!-- <tr>
                <td><b>Tanggal *</b></td>
                <td>
                  <input class="form-control" id="date" name="keranjang_tgl" placeholder="DD/MM/YYY" type="text"/>
                </td>
              </tr> -->
              <tr>
                <td><b>Keterangan</b></td>
                <td>
                  <textarea class="form-control" name="keterangan"></textarea>
                </td>
              </tr>
              <tr>
                  <td></td>
                  <td>
                    <div class="payment">
                    <button id="tombol-simpan" class="btn btn-primary" type="submit" name="addPemakaian"><i class="fa fa-save"></i> Simpan</button>
                    </div>
                  </td>
              </tr>
            </table>
          </div>
        </div>
      </div>
      </div>
      </form>
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<?php
  include_once "_template_bawah.php";
?>
<!-- DataTables -->
<script>

  function insertKeranjang(id){
    var brgKode = document.getElementById("bahanbaku_kode_"+id).value;
    var brgNama = document.getElementById("bahanbaku_nama_"+id).value;
    var brgHarga = document.getElementById("bahanbaku_harga_"+id).value;
    var dataQty = document.getElementById("keranjang_qty_"+id);
    console.log('sdfs');
    if(dataQty != undefined){
      document.getElementById("keranjang_qty_"+id).value = parseInt(dataQty.value) + 1;
      hitungSemua()
      return true;
    }

    var tbodyRef = document.getElementById('keranjang').getElementsByTagName('tbody')[0];

    // Insert a row at the end of table
    var newRow = tbodyRef.insertRow();
    var td = newRow.insertCell();
    var tdText = document.createTextNode(brgKode);
    td.appendChild(tdText);
    var td = newRow.insertCell();
    var tdText = document.createTextNode(brgNama);
    td.appendChild(tdText);

    var td = newRow.insertCell();
    td.style.textAlign = "right";
    var hargaString = formatAngka(brgHarga);
    var tdText = document.createTextNode(hargaString);
    td.appendChild(tdText);

    var td = newRow.insertCell();
    var input_qty = document.createElement('input');
    input_qty.setAttribute("type", "number");
    input_qty.setAttribute("min", "1");
    input_qty.setAttribute("size", "5");
    input_qty.setAttribute("value", "1");
    input_qty.setAttribute("required", "true");
    input_qty.setAttribute("onkeyup", "hitungSemua()");
    input_qty.setAttribute("onchange", "hitungSemua()");
    input_qty.setAttribute("id", "keranjang_qty_"+id);
    input_qty.setAttribute("name", "keranjang_qty["+id+"]");
    td.appendChild(input_qty);

    var td = newRow.insertCell();
    td.style.textAlign = "right";
    var div_sub = document.createElement('div');
    div_sub.setAttribute("id", "keranjang_sub_"+id);
    div_sub.innerHTML = formatAngka(brgHarga);
    td.appendChild(div_sub);

    var td = newRow.insertCell();
    var button_del = document.createElement('button');
    button_del.setAttribute("type", "button");
    button_del.setAttribute("class", "btn btn-xs btn-danger");
    button_del.setAttribute("onclick", "deleteKeranjang(this)");
    button_del.setAttribute("value", "Hapus");
    button_del.innerHTML = "<i class='fa fa-trash'></i>";
    td.appendChild(button_del);
    hitungSemua()
  }
  function deleteKeranjang(btn) {
    var row = btn.parentNode.parentNode;
    row.parentNode.removeChild(row);
    hitungSemua()
  }
  function hitungSemua(){
    var arrKeranjang = $('input[name^="keranjang_qty"]');
    if(arrKeranjang.length > 0){
      var total = 0;
      for(var i=0;i<arrKeranjang.length;i++){
        let id = arrKeranjang[i].id;
        id = id.replace("keranjang_qty_", "");

        let qty = arrKeranjang[i].value;
        let stok = document.getElementById("bahanbaku_stok_"+id).value;
        qty = parseInt(qty);
        stok = parseInt(stok);

        if(stok < qty){
          document.getElementById("keranjang_qty_"+id).value = stok;
          alert('melebihi stok yang tersedia');
          return;
        }

        let harga = document.getElementById("bahanbaku_harga_"+id).value;
        let subtotal = parseFloat(harga) * parseInt(qty);
        document.getElementById("keranjang_sub_"+id).innerHTML = formatAngka(subtotal);
        total = total + subtotal;
      }
      document.getElementById("keranjang_total_label").value = formatAngka(total);
      document.getElementById("keranjang_total").value = total;
    }
  }
  function formatAngka(x) {
      return x.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ".");
  }
  function validateMyForm(event){
    var arrKeranjang = $('input[name^="keranjang_qty"]');
    if(arrKeranjang.length > 0){
      return true;
    }else{
      event.preventDefault();
      alert("Keranjang masih kosong");
      return false;
    }
  }

  window.setTimeout(function() {
      $(".alert").fadeTo(1000, 0).slideUp(1000, function(){
          $(this).remove();
      });
  }, 10000);

  var date_input=$('input[name="keranjang_tgl"]'); //our date input has the name "date"
  var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
  var options={
    format: 'dd/mm/yyyy',
    container: container,
    todayHighlight: true,
    autoclose: true,
  };
  date_input.datepicker(options);

</script>
