<?php
  include_once "_template_atas.php";
?>

<?php
  if(isset($_POST['id']))
    $id = $_POST['id'];
  else
    $id = $_GET['id'];

  $dataKaryawan = query("SELECT * FROM karyawan WHERE kd_karyawan = '$id' ")[0];
  if(empty($dataKaryawan)){
    echo "<script>document.location.href = 'karyawan.php';</script>";
    die();
  }

  $err = '';
  if(isset($_POST['simpan'])){
    $nama    = trim($_POST['nama']);
    $username = trim($_POST['username']);
    $password = trim($_POST['password']);
    $password = md5($password);

    if( $nama == '' || $password == ''){
      $err = 'Kode karyawan, nama karyawan dan password wajib diisi';
    }else{
      $sql = " UPDATE `karyawan` SET
                 `nama_karyawan` = '$nama',
                 `username` = '$username',
                 `password` = '$password'
                WHERE
                `kd_karyawan` = '$id'
             ";
      $result = mysqli_query($conn, $sql);
      if($result === true){
        $_SESSION['sukses'] = 'Data berhasil disimpan';
        echo "<script>document.location.href = 'karyawan.php';</script>";
        die();
      }else{
        $err = 'Gagal menyimpan data';
      }
    }
  }
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Karyawan</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
            <li class="breadcrumb-item active">Karyawan</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">

      <div class="row">
        <div class="col-6 col-md-6  col-sm-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Ubah Karyawan</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <?php if(!empty($err)) { ?>
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                  <?= $err ?>
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
              <?php } ?>

              <form method="POST">
                <div class="form-group">
                  <label for="exampleInputEmail1">Kode Karyawan</label>
                  <input disabled type="text" name="kode" class="form-control" placeholder="Kode karyawan" value="<?= $dataKaryawan['kd_karyawan'] ?>">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Nama Karyawan</label>
                  <input type="text" name="nama" class="form-control" placeholder="Nama Karyawan" value="<?= $dataKaryawan['nama_karyawan'] ?>">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Username</label>
                  <input type="text" name="username" class="form-control" placeholder="Username" value="<?= $dataKaryawan['username'] ?>">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Password Baru</label>
                  <input type="text" name="password" class="form-control" placeholder="Password" value="">
                </div>
                <input type="hidden" name="id" value="<?= $id ?>">
                <button type="submit" name="simpan" class="btn btn-primary">Simpan</button>
                <a href="karyawan.php" class="btn btn-default">Batal</a>
              </form>
            </div>
            <!-- /.card-body -->
          </div>
        </div>
      </div>

    </div>
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<?php
  include_once "_template_bawah.php";
?>
