<?php
  include_once "_template_atas.php";
?>

<?php
  if(isset($_POST['id']))
    $id = $_POST['id'];
  else
    $id = $_GET['id'];

  $dataSupplier = query("SELECT * FROM supplier WHERE kd_supplier = '$id' ")[0];
  if(empty($dataSupplier)){
    echo "<script>document.location.href = 'supplier.php';</script>";
    die();
  }

  $err = '';
  if(isset($_POST['simpan'])){
    $nama    = trim($_POST['nama']);
    $telepon = trim($_POST['telepon']);
    $alamat  = trim($_POST['alamat']);

    if($nama == ''){
      $err = 'Kode supplier dan nama supplier wajib diisi';
    }else{
      $sql = " UPDATE `supplier` SET
                 `nama_supplier` = '$nama',
                 `no_telepon` = '$telepon',
                 `alamat` = '$alamat'
                WHERE
                `kd_supplier` = '$id'
             ";
      $result = mysqli_query($conn, $sql);
      if($result === true){
        $_SESSION['sukses'] = 'Data berhasil disimpan';
        echo "<script>document.location.href = 'supplier.php';</script>";
        die();
      }else{
        $err = 'Gagal menyimpan data';
      }
    }
  }
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Supplier</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
            <li class="breadcrumb-item active">Supplier</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">

      <div class="row">
        <div class="col-6 col-md-6  col-sm-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Ubah Supplier</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <?php if(!empty($err)) { ?>
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                  <?= $err ?>
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
              <?php } ?>

              <form method="POST">
                <div class="form-group">
                  <label for="exampleInputEmail1">Kode Supplier</label>
                  <input disabled type="text" name="kode" class="form-control" placeholder="Kode supplier" value="<?= $dataSupplier['kd_supplier'] ?>">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Nama Supplier</label>
                  <input type="text" name="nama" class="form-control" placeholder="Nama Supplier" value="<?= $dataSupplier['nama_supplier'] ?>">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">No Telepon</label>
                  <input type="text" name="telepon" class="form-control" placeholder="Nama Supplier" value="<?= $dataSupplier['no_telepon'] ?>">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Alamat</label>
                  <input type="text" name="alamat" class="form-control" placeholder="Nama Supplier" value="<?= $dataSupplier['alamat'] ?>">
                </div>
                <input type="hidden" name="id" value="<?= $id ?>">
                <button type="submit" name="simpan" class="btn btn-primary">Simpan</button>
                <a href="supplier.php" class="btn btn-default">Batal</a>
              </form>
            </div>
            <!-- /.card-body -->
          </div>
        </div>
      </div>

    </div>
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<?php
  include_once "_template_bawah.php";
?>
