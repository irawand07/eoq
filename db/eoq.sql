/*
SQLyog Ultimate v13.1.1 (64 bit)
MySQL - 5.7.33-0ubuntu0.18.04.1 : Database - eoq
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `bahan_baku` */

DROP TABLE IF EXISTS `bahan_baku`;

CREATE TABLE `bahan_baku` (
  `kd_bahanbaku` varchar(12) NOT NULL,
  `nama_bahanbaku` varchar(255) DEFAULT NULL,
  `satuan` char(12) DEFAULT NULL,
  `harga_satuan` decimal(20,2) DEFAULT '0.00',
  `stok_akhir` int(11) DEFAULT '0',
  PRIMARY KEY (`kd_bahanbaku`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `bahan_baku` */

insert  into `bahan_baku`(`kd_bahanbaku`,`nama_bahanbaku`,`satuan`,`harga_satuan`,`stok_akhir`) values 
('001','sdjfh sf sdf','Kg',23333.33,2),
('002','sirup','Pcs',32500.00,3),
('003','kecap','Kg',0.00,0);

/*Table structure for table `detail_pemakaian` */

DROP TABLE IF EXISTS `detail_pemakaian`;

CREATE TABLE `detail_pemakaian` (
  `id_detpemakaian` int(11) NOT NULL AUTO_INCREMENT,
  `id_pemakaian` int(11) DEFAULT NULL,
  `kd_bahanbaku` varchar(12) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `harga` decimal(20,2) DEFAULT NULL,
  `subtotal` decimal(20,2) DEFAULT NULL,
  PRIMARY KEY (`id_detpemakaian`),
  KEY `id_pemakaian` (`id_pemakaian`),
  KEY `kd_bahanbaku` (`kd_bahanbaku`),
  CONSTRAINT `detail_pemakaian_ibfk_1` FOREIGN KEY (`id_pemakaian`) REFERENCES `pemakaian` (`id_pemakaian`),
  CONSTRAINT `detail_pemakaian_ibfk_2` FOREIGN KEY (`kd_bahanbaku`) REFERENCES `bahan_baku` (`kd_bahanbaku`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

/*Data for the table `detail_pemakaian` */

insert  into `detail_pemakaian`(`id_detpemakaian`,`id_pemakaian`,`kd_bahanbaku`,`qty`,`harga`,`subtotal`) values 
(11,9,'001',1,23333.33,23333.33),
(12,9,'002',1,32500.00,32500.00);

/*Table structure for table `detail_pembelian` */

DROP TABLE IF EXISTS `detail_pembelian`;

CREATE TABLE `detail_pembelian` (
  `id_detbeli` int(11) NOT NULL AUTO_INCREMENT,
  `kd_bahanbaku` varchar(12) DEFAULT NULL,
  `id_beli` int(11) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `harga_beli` int(11) DEFAULT NULL,
  `subtotal` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_detbeli`),
  KEY `kd_bahanbaku` (`kd_bahanbaku`),
  KEY `id_beli` (`id_beli`),
  CONSTRAINT `detail_pembelian_ibfk_1` FOREIGN KEY (`kd_bahanbaku`) REFERENCES `bahan_baku` (`kd_bahanbaku`),
  CONSTRAINT `detail_pembelian_ibfk_2` FOREIGN KEY (`id_beli`) REFERENCES `pembelian` (`id_beli`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

/*Data for the table `detail_pembelian` */

insert  into `detail_pembelian`(`id_detbeli`,`kd_bahanbaku`,`id_beli`,`qty`,`harga_beli`,`subtotal`) values 
(15,'001',6,2,30000,60000),
(16,'002',6,3,40000,120000),
(17,'001',7,1,10000,10000),
(18,'002',7,1,10000,10000);

/*Table structure for table `detail_perhitungan` */

DROP TABLE IF EXISTS `detail_perhitungan`;

CREATE TABLE `detail_perhitungan` (
  `id_detperhitungan` int(11) NOT NULL AUTO_INCREMENT,
  `id_perhitungan` int(11) DEFAULT NULL,
  `kd_bahanbaku` varchar(12) DEFAULT NULL,
  `harga_satuan` decimal(20,2) DEFAULT NULL,
  `biaya_pesan` decimal(20,2) DEFAULT NULL,
  `biaya_simpan` decimal(20,2) DEFAULT NULL,
  `kebutuhan_perhari` decimal(20,2) DEFAULT NULL,
  `kebutuhan_pertahun` decimal(20,2) DEFAULT NULL,
  `lead_time` decimal(20,2) DEFAULT NULL,
  `safety_stok` decimal(20,2) DEFAULT NULL,
  `eoq` decimal(20,2) DEFAULT NULL,
  `reorder_poin` decimal(20,2) DEFAULT NULL,
  `frekuensi` decimal(20,2) DEFAULT NULL,
  PRIMARY KEY (`id_detperhitungan`),
  KEY `id_perhitungan` (`id_perhitungan`),
  KEY `kd_bahanbaku` (`kd_bahanbaku`),
  CONSTRAINT `detail_perhitungan_ibfk_1` FOREIGN KEY (`id_perhitungan`) REFERENCES `perhitungan` (`id_perhitungan`) ON UPDATE CASCADE,
  CONSTRAINT `detail_perhitungan_ibfk_2` FOREIGN KEY (`kd_bahanbaku`) REFERENCES `bahan_baku` (`kd_bahanbaku`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `detail_perhitungan` */

insert  into `detail_perhitungan`(`id_detperhitungan`,`id_perhitungan`,`kd_bahanbaku`,`harga_satuan`,`biaya_pesan`,`biaya_simpan`,`kebutuhan_perhari`,`kebutuhan_pertahun`,`lead_time`,`safety_stok`,`eoq`,`reorder_poin`,`frekuensi`) values 
(5,4,'003',NULL,50000.00,10.00,40.00,11520.00,2.00,37.00,1959.59,480.00,6.00),
(6,5,'001',10000.00,30000.00,5.00,50.00,14400.00,2.00,46.00,1314.53,600.00,11.00);

/*Table structure for table `karyawan` */

DROP TABLE IF EXISTS `karyawan`;

CREATE TABLE `karyawan` (
  `kd_karyawan` varchar(12) NOT NULL,
  `nama_karyawan` varchar(30) DEFAULT NULL,
  `username` varchar(30) DEFAULT NULL,
  `password` varchar(60) DEFAULT NULL,
  `hak_akses` enum('admin','pemilik') DEFAULT NULL,
  PRIMARY KEY (`kd_karyawan`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `karyawan` */

insert  into `karyawan`(`kd_karyawan`,`nama_karyawan`,`username`,`password`,`hak_akses`) values 
('001','Pemilik','pemilik','58399557dae3c60e23c78606771dfa3d','pemilik'),
('002','Admin','admin','21232f297a57a5a743894a0e4a801fc3','admin');

/*Table structure for table `pemakaian` */

DROP TABLE IF EXISTS `pemakaian`;

CREATE TABLE `pemakaian` (
  `id_pemakaian` int(11) NOT NULL AUTO_INCREMENT,
  `no_pemakaian` varchar(10) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `total` decimal(20,2) DEFAULT NULL,
  `kd_karyawan` varchar(12) DEFAULT NULL,
  `keterangan` text,
  PRIMARY KEY (`id_pemakaian`),
  UNIQUE KEY `no_pemakaian` (`no_pemakaian`),
  KEY `kd_karyawan` (`kd_karyawan`),
  CONSTRAINT `pemakaian_ibfk_1` FOREIGN KEY (`kd_karyawan`) REFERENCES `karyawan` (`kd_karyawan`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

/*Data for the table `pemakaian` */

insert  into `pemakaian`(`id_pemakaian`,`no_pemakaian`,`tanggal`,`total`,`kd_karyawan`,`keterangan`) values 
(9,'PKA000001','2021-04-25 00:08:31',55833.33,'002','pemakaian 1');

/*Table structure for table `pembelian` */

DROP TABLE IF EXISTS `pembelian`;

CREATE TABLE `pembelian` (
  `id_beli` int(11) NOT NULL AUTO_INCREMENT,
  `no_pembelian` varchar(10) DEFAULT NULL,
  `kd_supplier` varchar(12) DEFAULT NULL,
  `tanggal_beli` datetime DEFAULT NULL,
  `total` decimal(20,2) DEFAULT NULL,
  `kd_karyawan` varchar(12) DEFAULT NULL,
  `keterangan` text,
  PRIMARY KEY (`id_beli`),
  UNIQUE KEY `no_pembelian` (`no_pembelian`),
  KEY `kd_supplier` (`kd_supplier`),
  KEY `kd_karyawan` (`kd_karyawan`),
  CONSTRAINT `pembelian_ibfk_1` FOREIGN KEY (`kd_supplier`) REFERENCES `supplier` (`kd_supplier`),
  CONSTRAINT `pembelian_ibfk_2` FOREIGN KEY (`kd_karyawan`) REFERENCES `karyawan` (`kd_karyawan`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `pembelian` */

insert  into `pembelian`(`id_beli`,`no_pembelian`,`kd_supplier`,`tanggal_beli`,`total`,`kd_karyawan`,`keterangan`) values 
(6,'INV000001','006','2021-04-24 22:41:01',180000.00,'002','pertama'),
(7,'INV000002','006','2021-04-24 22:42:12',20000.00,'002','kedua');

/*Table structure for table `perhitungan` */

DROP TABLE IF EXISTS `perhitungan`;

CREATE TABLE `perhitungan` (
  `id_perhitungan` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal` datetime DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `kd_karyawan` varchar(12) DEFAULT NULL,
  PRIMARY KEY (`id_perhitungan`),
  KEY `kd_karyawan` (`kd_karyawan`),
  CONSTRAINT `perhitungan_ibfk_1` FOREIGN KEY (`kd_karyawan`) REFERENCES `karyawan` (`kd_karyawan`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `perhitungan` */

insert  into `perhitungan`(`id_perhitungan`,`tanggal`,`keterangan`,`kd_karyawan`) values 
(4,'2021-04-28 15:23:06','oke3','002'),
(5,'2021-04-28 15:44:46','yes','002');

/*Table structure for table `persediaan` */

DROP TABLE IF EXISTS `persediaan`;

CREATE TABLE `persediaan` (
  `id_persediaan` int(11) NOT NULL AUTO_INCREMENT,
  `id_detbeli` int(11) DEFAULT NULL,
  `id_detpemakaian` int(11) DEFAULT NULL,
  `kd_bahanbaku` varchar(12) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `harga` decimal(20,2) DEFAULT '0.00',
  `total` decimal(20,2) DEFAULT '0.00',
  PRIMARY KEY (`id_persediaan`),
  KEY `id_detbeli` (`id_detbeli`),
  KEY `id_detpemakaian` (`id_detpemakaian`),
  KEY `kd_bahanbaku` (`kd_bahanbaku`),
  CONSTRAINT `persediaan_ibfk_1` FOREIGN KEY (`id_detbeli`) REFERENCES `detail_pembelian` (`id_detbeli`),
  CONSTRAINT `persediaan_ibfk_2` FOREIGN KEY (`kd_bahanbaku`) REFERENCES `bahan_baku` (`kd_bahanbaku`),
  CONSTRAINT `persediaan_ibfk_3` FOREIGN KEY (`id_detpemakaian`) REFERENCES `detail_pemakaian` (`id_detpemakaian`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

/*Data for the table `persediaan` */

insert  into `persediaan`(`id_persediaan`,`id_detbeli`,`id_detpemakaian`,`kd_bahanbaku`,`tanggal`,`qty`,`harga`,`total`) values 
(2,15,NULL,'001','2021-04-24 22:41:01',2,30000.00,60000.00),
(3,16,NULL,'002','2021-04-24 22:41:01',3,40000.00,120000.00),
(4,17,NULL,'001','2021-04-24 22:42:12',3,23333.33,70000.00),
(5,18,NULL,'002','2021-04-24 22:42:12',4,32500.00,130000.00),
(15,NULL,11,'001','2021-04-25 00:08:31',2,23333.33,46666.66),
(16,NULL,12,'002','2021-04-25 00:08:31',3,32500.00,97500.00);

/*Table structure for table `supplier` */

DROP TABLE IF EXISTS `supplier`;

CREATE TABLE `supplier` (
  `kd_supplier` varchar(12) NOT NULL,
  `nama_supplier` varchar(255) DEFAULT NULL,
  `no_telepon` char(13) DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`kd_supplier`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `supplier` */

insert  into `supplier`(`kd_supplier`,`nama_supplier`,`no_telepon`,`alamat`) values 
('001','Paijo royoo','0834590','sdkjfj sd sjdfk jsdko'),
('002','JHsdafh','02398483','jsdfhsd hfsdh'),
('003','sdkfjj','02384923','sjdhf sdhfjsd'),
('004','sjdhfjskd','08238747','alasmnsdjf'),
('006','jasjhda j','023842398','jsdf kjs dfhjk');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
