<?php
  include_once "_template_atas.php";
?>

<?php
  
  // increment kode otomatis
  $increKode = query("SELECT LPAD( IFNULL(MAX(kd_supplier),0)+1 ,3,'0') AS kode FROM supplier")[0];

  $err = '';
  if(isset($_POST['simpan'])){
    $kode    = trim($_POST['kode']);
    $nama    = trim($_POST['nama']);
    $telepon = trim($_POST['telepon']);
    $alamat  = trim($_POST['alamat']);

    if($kode == '' || $nama == ''){
      $err = 'Kode supplier dan nama supplier wajib diisi';
    }else{
      $sql = " INSERT INTO `supplier` (`kd_supplier`, `nama_supplier`, `no_telepon`, `alamat`)
               VALUES ('$kode', '$nama', '$telepon', '$alamat')
             ";
      $result = mysqli_query($conn, $sql);
      if($result === true){
        $_SESSION['sukses'] = 'Data berhasil disimpan';
        echo "<script>document.location.href = 'supplier.php';</script>";
        die();
      }else{
        $err = 'Gagal menyimpan data';
      }
    }

  }
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Supplier</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
            <li class="breadcrumb-item active">Supplier</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">

      <div class="row">
        <div class="col-6 col-md-6  col-sm-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Tambah Supplier</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <?php if(!empty($err)) { ?>
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                  <?= $err ?>
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
              <?php } ?>

              <form method="POST">
                <div class="form-group">
                  <label for="exampleInputEmail1">Kode Supplier</label>
                  <input type="text" name="kode" class="form-control" placeholder="Kode supplier" value="<?= $increKode['kode'] ?>">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Nama Supplier</label>
                  <input type="text" name="nama" class="form-control" placeholder="Nama Supplier">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">No Telepon</label>
                  <input type="text" name="telepon" class="form-control" placeholder="Nama Supplier">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Alamat</label>
                  <input type="text" name="alamat" class="form-control" placeholder="Nama Supplier">
                </div>
                <button type="submit" name="simpan" class="btn btn-primary">Simpan</button>
                <a href="supplier.php" class="btn btn-default">Batal</a>
              </form>
            </div>
            <!-- /.card-body -->
          </div>
        </div>
      </div>

    </div>
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<?php
  include_once "_template_bawah.php";
?>
