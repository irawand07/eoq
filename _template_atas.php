<?php
  include 'aksi/halau.php';
  include 'aksi/functions.php';

  $menuActive = basename($_SERVER['REQUEST_URI'], '?' . $_SERVER['QUERY_STRING']);
  $menuActive = str_replace('.php','',$menuActive);

  $arrMenuKaryawan = array('karyawan','karyawanTambah','karyawanUbah','karyawanHapus');
  $arrMenuSupplier = array('supplier','supplierTambah','supplierUbah','supplierHapus');
  $arrMenuBahanBaku = array('bahanbaku','bahanbakuTambah','bahanbakuUbah','bahanbakuHapus');
  $arrMenuPembelian = array('pembelian');
  $arrMenuPemakaian = array('pemakaian');
  $arrMenuPerhitungan = array('perhitungan','perhitunganTambah','perhitunganDetail');

  $arrMenuLaporanSupplier = array('lap_supplier');
  $arrMenuLaporanBahanbaku = array('lap_bahanbaku');
  $arrMenuLaporanPembelian = array('lap_pembelian', 'lap_pembelian_detail');
  $arrMenuLaporanPemakaian = array('lap_pemakaian', 'lap_pemakaian_detail');
  $arrMenuLaporanKartuPersediaan = array('lap_kartu_persediaan');
  $arrMenuLaporanHasilPerhitungan = array('lap_hasil_perhitungan');
  $arrMenuLaporan = array_merge($arrMenuLaporanSupplier,$arrMenuLaporanBahanbaku,$arrMenuLaporanPembelian,$arrMenuLaporanPemakaian,$arrMenuLaporanKartuPersediaan,$arrMenuLaporanHasilPerhitungan);

  $hakAkses = $_SESSION['u_akses'];

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Rumah ANNA</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bootstrap 4 -->
  <link rel="stylesheet" href="plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- JQVMap -->
  <link rel="stylesheet" href="plugins/jqvmap/jqvmap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker.css">
  <!-- summernote -->
  <link rel="stylesheet" href="plugins/summernote/summernote-bs4.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
  <link rel="stylesheet" href="plugins/jquery-ui/jquery-ui.min.css">
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <li class="nav-item">
        <a href="aksi/logout.php?logout" onclick="return confirm('Apakah anda yakin logout ?')" class="nav-link">Logout <i class="fas fa-sign-out-alt"></i></a>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index.php" class="brand-link">
      <img src="dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">Rumah ANNA</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">

      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="dist/img/avatar4.png" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block"><?= $_SESSION['u_nama'] ?></a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <?php if($hakAkses == 'admin'){ ?>
          <li class="nav-item">
            <a href="supplier.php" class="nav-link <?php if(in_array($menuActive,$arrMenuSupplier)) echo 'active'; ?>">
              <i class="nav-icon fas fa-address-book"></i>
              <p>Supplier</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="bahanbaku.php" class="nav-link <?php if(in_array($menuActive,$arrMenuBahanBaku)) echo 'active'; ?>">
              <i class="nav-icon fas fa-archive"></i>
              <p>Bahan Baku</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="pembelian.php" class="nav-link <?php if(in_array($menuActive,$arrMenuPembelian)) echo 'active'; ?>">
              <i class="nav-icon fas fa-shopping-cart"></i>
              <p>Pembelian</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="pemakaian.php" class="nav-link <?php if(in_array($menuActive,$arrMenuPemakaian)) echo 'active'; ?>">
              <i class="nav-icon fas fa-boxes"></i>
              <p>Pemakaian</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="perhitungan.php" class="nav-link <?php if(in_array($menuActive,$arrMenuPerhitungan)) echo 'active'; ?>">
              <i class="nav-icon fas fa-calculator"></i>
              <p>Perhitungan</p>
            </a>
          </li>
          <?php } ?>

          <?php if($hakAkses == 'pemilik'){ ?>
          <li class="nav-item">
            <a href="karyawan.php" class="nav-link <?php if(in_array($menuActive,$arrMenuKaryawan)) echo 'active'; ?>">
              <i class="nav-icon fas fa-users"></i>
              <p>Karyawan</p>
            </a>
          </li>
          <?php } ?>

          <li class="nav-item <?php if(in_array($menuActive,$arrMenuLaporan)) echo ' menu-open'; ?>">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-table"></i>
              <p>
                Laporan
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="lap_supplier.php" class="nav-link <?php if(in_array($menuActive,$arrMenuLaporanSupplier)) echo 'active'; ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Supplier</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="lap_bahanbaku.php" class="nav-link <?php if(in_array($menuActive,$arrMenuLaporanBahanbaku)) echo 'active'; ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Bahan baku</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="lap_pembelian.php" class="nav-link <?php if(in_array($menuActive,$arrMenuLaporanPembelian)) echo 'active'; ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Pembelian</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="lap_pemakaian.php" class="nav-link <?php if(in_array($menuActive,$arrMenuLaporanPemakaian)) echo 'active'; ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Pemakaian</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="lap_hasil_perhitungan.php" class="nav-link <?php if(in_array($menuActive,$arrMenuLaporanHasilPerhitungan)) echo 'active'; ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Hasil Perhitungan</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="lap_kartu_persediaan.php" class="nav-link <?php if(in_array($menuActive,$arrMenuLaporanKartuPersediaan)) echo 'active'; ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Kartu Persediaan</p>
                </a>
              </li>
            </ul>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
