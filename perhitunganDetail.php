<?php
  include_once "_template_atas.php";
?>

<?php

  $id = $_GET['id'];

  $dataPerhitungan = query("
    SELECT
      id_perhitungan,
      DATE_FORMAT(perhitungan.`tanggal`, '%d-%m-%Y %H:%i') AS tgl,
      perhitungan.keterangan,
      karyawan.`nama_karyawan`
    FROM perhitungan
    LEFT JOIN karyawan ON perhitungan.`kd_karyawan` = karyawan.`kd_karyawan`
    WHERE id_perhitungan = '$id'
  ")[0];

  $detail = query("
    SELECT
      bahan_baku.kd_bahanbaku,
      bahan_baku.nama_bahanbaku,
      detail_perhitungan.harga_satuan,
      biaya_pesan,
      biaya_simpan,
      kebutuhan_perhari,
      kebutuhan_pertahun,
      lead_time,
      safety_stok,
      eoq,
      reorder_poin,
      frekuensi
    FROM detail_perhitungan
    LEFT JOIN bahan_baku ON bahan_baku.kd_bahanbaku = detail_perhitungan.kd_bahanbaku
    WHERE id_perhitungan = '$id'
  ");
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Perhitungan Detail</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
            <li class="breadcrumb-item ">Perhitungan</li>
            <li class="breadcrumb-item active">Detail</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">

      <div class="row">
        <div class="col-6">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Perhitungan</h3>
            </div>
            <div class="card-body">
              <table width="100%">
                <tr>
                  <td width="170px">Tanggal Perhitungan</td>
                  <td width="1%">: </td>
                  <td><?= $dataPerhitungan['tgl'] ?></td>
                </tr>
                <tr>
                  <td>Keterangan</td>
                  <td>: </td>
                  <td><?= $dataPerhitungan['keterangan'] ?></td>
                </tr>
                <tr>
                  <td>Karyawan</td>
                  <td>: </td>
                  <td><?= $dataPerhitungan['nama_karyawan'] ?></td>
                </tr>
              </table>
            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Detail </h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table class="table table-bordered" width="100%">
                <thead>
                  <th width="50px">No</th>
                  <th>Nama</th>
                  <th>Harga</th>
                  <th>Biaya Pesan</th>
                  <th>Biaya Simpan</th>
                  <th>Leadtime</th>
                  <th>Kebutuhan per Tahun</th>
                  <th>EOQ</th>
                  <th>SS</th>
                  <th>ROP</th>
                  <th>Frek</th>
                </thead>
                <tbody>
                <?php
                  if(!empty($detail)){
                    $no = 1;
                    foreach($detail as $row){
                      echo "<tr>";
                      echo "<td>".$no."</td>";
                      echo "<td>".$row['nama_bahanbaku']."</td>";
                      echo "<td>".number_format($row['harga_satuan'],2,',','.')."</td>";
                      echo "<td>".number_format($row['biaya_pesan'],2,',','.')."</td>";
                      echo "<td>".number_format($row['biaya_simpan'],0,',','.')."</td>";
                      echo "<td>".number_format($row['lead_time'],0,',','.')."</td>";
                      echo "<td>".number_format($row['kebutuhan_pertahun'],0,',','.')."</td>";
                      echo "<td>".number_format($row['eoq'],2,',','.')."</td>";
                      echo "<td>".number_format($row['safety_stok'],2,',','.')."</td>";
                      echo "<td>".number_format($row['reorder_poin'],2,',','.')."</td>";
                      echo "<td>".number_format($row['frekuensi'],2,',','.')."</td>";
                      echo "</tr>";
                      $no++;
                    }
                  }
                ?>
                </tbody>
              </table>
              <br/>
              <a href="perhitungan.php" class="btn btn-default">Kembali</a>
            </div>
            <!-- /.card-body -->
          </div>
        </div>
      </div>

    </div>
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<?php
  include_once "_template_bawah.php";
?>

<script>
// $('.datepicker').datepicker({
//    format: 'dd/mm/yyyy'
// });
var date_input=$('input[name="tgl_awal"]'); //our date input has the name "date"
var date_input2=$('input[name="tgl_akhir"]'); //our date input has the name "date"
var options={
  dateFormat: 'dd-mm-yy',
  todayHighlight: true,
  autoclose: true,
};
date_input.datepicker(options);
date_input2.datepicker(options);

$('input[name="tgl_awal"]').datepicker("setDate", '<?= $tglAwalLabel ?>' );
$('input[name="tgl_akhir"]').datepicker("setDate", '<?= $tglAkhirLabel ?>' );
</script>
