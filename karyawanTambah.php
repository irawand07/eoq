<?php
  include_once "_template_atas.php";
?>

<?php
  $err = '';
  if(isset($_POST['simpan'])){
    $kode    = trim($_POST['kode']);
    $nama    = trim($_POST['nama']);
    $username = trim($_POST['username']);
    $password = trim($_POST['password']);
    $password = md5($password);

    if($kode == '' || $nama == '' || $password == ''){
      $err = 'Kode karyawan, nama karyawan dan password wajib diisi';
    }else{
      $sql = " INSERT INTO `karyawan` (`kd_karyawan`, `nama_karyawan`, `username`, `password`, `hak_akses`)
               VALUES ('$kode', '$nama', '$username', '$password', 'admin')
             ";
      $result = mysqli_query($conn, $sql);
      if($result === true){
        $_SESSION['sukses'] = 'Data berhasil disimpan';
        echo "<script>document.location.href = 'karyawan.php';</script>";
        die();
      }else{
        $err = 'Gagal menyimpan data';
      }
    }

  }
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Karyawan</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
            <li class="breadcrumb-item active">Karyawan</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">

      <div class="row">
        <div class="col-6 col-md-6  col-sm-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Tambah Karyawan</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <?php if(!empty($err)) { ?>
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                  <?= $err ?>
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
              <?php } ?>

              <form method="POST">
                <div class="form-group">
                  <label for="exampleInputEmail1">Kode Karyawan</label>
                  <input type="text" name="kode" class="form-control" placeholder="Kode karyawan">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Nama Karyawan</label>
                  <input type="text" name="nama" class="form-control" placeholder="Nama Karyawan">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Username</label>
                  <input type="text" name="username" class="form-control" placeholder="Username">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Password</label>
                  <input type="password" name="password" class="form-control" placeholder="Password">
                </div>
                <button type="submit" name="simpan" class="btn btn-primary">Simpan</button>
                <a href="karyawan.php" class="btn btn-default">Batal</a>
              </form>
            </div>
            <!-- /.card-body -->
          </div>
        </div>
      </div>

    </div>
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<?php
  include_once "_template_bawah.php";
?>
