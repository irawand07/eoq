<?php
  include_once "_template_atas.php";
?>

<?php
  if(isset($_SESSION['sukses'])){
  }

  $hasilPerhitungan = array();
  if(isset($_SESSION['perhitungan'])){
    $hasilPerhitungan = $_SESSION['perhitungan'];
  }

  if(!empty($hasilPerhitungan) && isset($_POST['batal'])){
    unset($_SESSION['perhitungan']);
    echo "<script>document.location.href = 'perhitungan.php';</script>";
    die();
  }

  if(!empty($hasilPerhitungan) && isset($_POST['simpan'])){
    $tglPerhitungan = date('Y-m-d H:i:s');
    $keterangan = trim($_POST['keterangan']);
    $userId = $_SESSION['u_kode'];

    $conn->begin_transaction();
    //insert ke perhitungan
    $insertPerhitungan = $conn->query("
        INSERT INTO `perhitungan`
          ( `tanggal`, `kd_karyawan`,`keterangan`)
        VALUES
          ('$tglPerhitungan', '$userId', '$keterangan')
    ");

    $idPerhitungan = query("SELECT id_perhitungan FROM perhitungan WHERE tanggal = '$tglPerhitungan' LIMIT 1 ")[0]['id_perhitungan'];

    //insert ke detail
    $insertDet = true;
    foreach($hasilPerhitungan as $row){
      $bb              = $row['bahanbaku'];
      $harga           = $row['harga'];
      $biaya_pesan     = $row['biaya_pesan'];
      $biaya_simpan    = $row['biaya_simpan'];
      $leadtime        = $row['leadtime'];
      $kebutuhan       = $row['kebutuhan'];
      $kebutuhan_tahun = $row['kebutuhan_pertahun'];
      $eoq             = $row['eoq'];
      $ss              = $row['ss'];
      $rop             = $row['rop'];
      $fre             = $row['frekuensi'];

      $perhDetail = $conn->query("
          INSERT INTO `detail_perhitungan`
            (`id_perhitungan`, `kd_bahanbaku`, `harga_satuan`, `biaya_pesan`, `biaya_simpan`, `kebutuhan_perhari`, `kebutuhan_pertahun`, `lead_time`, `safety_stok`, `eoq`, `reorder_poin`, `frekuensi`)
          VALUES
          ('$idPerhitungan', '$bb', '$harga', '$biaya_pesan', '$biaya_simpan', '$kebutuhan', '$kebutuhan_tahun', '$leadtime', '$ss', '$eoq', '$rop', '$fre')
      ");

      $insertDet = $insertDet && $perhDetail;
    }

    $result = $insertDet && $insertPerhitungan;
    if(!$result){
      $conn->rollback();
      $err = 'Gagal menyimpan data';
    }else{
      $conn->commit();
      unset($_SESSION['perhitungan']);
      $_SESSION['sukses'] = 'Data berhasil disimpan';
      echo "<script>document.location.href = 'perhitungan.php';</script>";
      die();
    }

  }

?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Perhitungan</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
            <li class="breadcrumb-item">Perhitungan</li>
            <li class="breadcrumb-item active">Tambah</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">

      <div class="row">
        <div class="col-8">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Pemakaian</h3>
            </div>
            <div class="card-body">
              <?php if(!empty($err)) { ?>
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                  <?= $err ?>
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
              <?php } ?>
              <form method="post" action="perhitunganProses.php" >
                <div class="row">
                  <div class="col-6">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Bahan baku</label>
                      <select name="bahanbaku" class="form-control" required>
                        <option value=""></option>
                        <?php
                          $dataBahanbaku = $conn->query("
                            SELECT * FROM bahan_baku ORDER BY nama_bahanbaku
                          ");

                          foreach($dataBahanbaku as $row){
                            echo "<option value='".$row['kd_bahanbaku']."'>".$row['nama_bahanbaku']." (".$row['kd_bahanbaku'].")</optio>";
                          }
                        ?>
                      </select>
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Harga Satuan (Rp)</label>
                      <input min="0" type="number" name="harga" class="form-control" placeholder="Harga Satuan" required>
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Biaya Pesan (Rp)</label>
                      <input min="0" type="number" name="biaya_pesan" class="form-control" placeholder="Biaya Pesan" required>
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Biaya Simpan (%)</label>
                      <input min="0" type="number" name="biaya_simpan" class="form-control" placeholder="Biaya Simpan" required>
                    </div>
                    <button type="submit" name="tambah" class="btn btn-primary">Hitung</button>
                  </div>
                  <div class="col-6">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Leadtime (Hari)</label>
                      <input min="0" type="number" name="leadtime" class="form-control" placeholder="Leadtime" required>
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Kebutuhan per hari</label>
                      <input min="0" type="number" name="kebutuhan" class="form-control" placeholder="Kebutuhan" required>
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Hari kerja per tahun</label>
                      <input min="0" type="number" name="hari_kerja" class="form-control" placeholder="Kebutuhan" value="360" required>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Hasil Perhitungan</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">

              <table class="table table-bordered">
                <thead>
                <tr>
                  <th width="50px">No</th>
                  <th>Nama</th>
                  <th>Harga</th>
                  <th>Biaya Pesan</th>
                  <th>Biaya Simpan</th>
                  <th>Leadtime</th>
                  <th>Kebutuhan per Tahun</th>
                  <th>EOQ</th>
                  <th>SS</th>
                  <th>ROP</th>
                  <th>Frek</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                  <?php
                  if(!empty($hasilPerhitungan)){
                    $no = 1;
                    foreach($hasilPerhitungan as $row){
                      echo "<tr>";
                      echo "<td>".$no."</td>";
                      echo "<td>".$row['nama']."</td>";
                      echo "<td>".number_format($row['harga'],2,',','.')."</td>";
                      echo "<td>".number_format($row['biaya_pesan'],2,',','.')."</td>";
                      echo "<td>".$row['biaya_simpan']."</td>";
                      echo "<td>".$row['leadtime']."</td>";
                      echo "<td>".number_format($row['kebutuhan_pertahun'],0,',','.')."</td>";
                      echo "<td>".number_format($row['eoq'],2,',','.')."</td>";
                      echo "<td>".number_format($row['ss'],2,',','.')."</td>";
                      echo "<td>".number_format($row['rop'],2,',','.')."</td>";
                      echo "<td>".number_format($row['frekuensi'],2,',','.')."</td>";
                      echo "<td><a class='btn btn-xs btn-danger' onclick='return confirm(\"Hapus perhitungan ".$row['nama']." ?\")' href='perhitunganProsesHapus.php?id=".$row['bahanbaku']."'>Hapus</a></td>";
                      echo "</tr>";
                      $no++;
                    }
                  }
                  ?>
                </tbody>
              </table>

              <?php if(!empty($hasilPerhitungan)){ ?>
                <form method="post">
                  <div class="form-group col-4 mt-4 pl-0">
                    <label for="exampleInputEmail1">Tanggal</label>
                    <input type="text" class="form-control" name="tanggal" value="<?= date('Y-m-d') ?>" disabled>
                  </div>
                  <div class="form-group col-4 pl-0">
                    <label for="exampleInputEmail1">Keterangan</label>
                    <textarea name="keterangan" class="form-control"></textarea>
                  </div>
                  <button type="submit" name="simpan" class="btn btn-primary">Simpan</button>
                  <button type="submit" name="batal" class="btn btn-default">Batal</button>
                </form>
              <?php } ?>

            </div>
            <!-- /.card-body -->
          </div>
        </div>
      </div>

    </div>
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<?php
  include_once "_template_bawah.php";
?>
