<?php
  include_once "_template_atas.php";
?>

<?php

// increment kode otomatis
$increKode = query("SELECT LPAD( IFNULL(MAX(kd_bahanbaku),0)+1 ,3,'0') AS kode FROM bahan_baku")[0];

  $err = '';
  if(isset($_POST['simpan'])){
    $kode    = trim($_POST['kode']);
    $nama    = trim($_POST['nama']);
    $satuan  = trim($_POST['satuan']);

    if($kode == '' || $nama == ''){
      $err = 'Kode bahan baku dan nama bahan baku wajib diisi';
    }else{
      $sql = " INSERT INTO `bahan_baku` (`kd_bahanbaku`, `nama_bahanbaku`, `satuan`)
               VALUES ('$kode', '$nama', '$satuan')
             ";
      $result = mysqli_query($conn, $sql);
      if($result === true){
        $_SESSION['sukses'] = 'Data berhasil disimpan';
        echo "<script>document.location.href = 'bahanbaku.php';</script>";
        die();
      }else{
        $err = 'Gagal menyimpan data';
      }
    }

  }
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Bahan baku</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
            <li class="breadcrumb-item active">Bahan baku</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">

      <div class="row">
        <div class="col-6 col-md-6  col-sm-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Tambah Bahan baku</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <?php if(!empty($err)) { ?>
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                  <?= $err ?>
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
              <?php } ?>

              <form method="POST">
                <div class="form-group">
                  <label for="exampleInputEmail1">Kode Bahan baku</label>
                  <input type="text" name="kode" class="form-control" placeholder="Kode bahan baku" value="<?= $increKode['kode'] ?>">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Nama Bahan baku</label>
                  <input type="text" name="nama" class="form-control" placeholder="Nama Supplier">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Satuan</label>
                  <select name="satuan" class="form-control">
                    <option value="Pcs">Pcs</option>
                    <option value="Kg">Kg</option>
                  </select>
                </div>
                <button type="submit" name="simpan" class="btn btn-primary">Simpan</button>
                <a href="bahanbaku.php" class="btn btn-default">Batal</a>
              </form>
            </div>
            <!-- /.card-body -->
          </div>
        </div>
      </div>

    </div>
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<?php
  include_once "_template_bawah.php";
?>
