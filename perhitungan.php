<?php
  include_once "_template_atas.php";
?>

<?php
  $sukses = '';
  if(isset($_SESSION['sukses'])){
    $sukses = $_SESSION['sukses'];
    unset($_SESSION['sukses']);
  }

  $dataSupplier = $conn->query("
    SELECT
     id_perhitungan,
     DATE_FORMAT(tanggal,'%d-%m-%Y %H:%i') AS tgl,
     keterangan,
     karyawan.nama_karyawan
    FROM perhitungan
    LEFT JOIN karyawan ON perhitungan.kd_karyawan = karyawan.kd_karyawan
    ORDER BY id_perhitungan
  ");
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Perhitungan</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
            <li class="breadcrumb-item active">Perhitungan</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">

      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Daftar Perhitungan</h3>
              <a href="perhitunganTambah.php" style="float:right" class="btn btn-sm btn-primary"><i class='fas fa-plus'></i> Tambah</a>
            </div>
            <!-- /.card-header -->
            <div class="card-body">

              <?php if(!empty($sukses)) { ?>
                <div class="alert alert-info alert-dismissible fade show" role="alert">
                  <?= $sukses ?>
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
              <?php } ?>

              <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th width="50px">No</th>
                  <th>Tanggal</th>
                  <th>Keterangan</th>
                  <th>Nama Karyawan</th>
                  <th width="200px">Aksi</th>
                </tr>
                </thead>
                <tbody>
                  <?php
                    if(!empty($dataSupplier)){
                      $no = 1;
                      foreach($dataSupplier as $row){
                        echo "<tr>";
                        echo "<td>".$no."</td>";
                        echo "<td>".$row['tgl']."</td>";
                        echo "<td>".$row['keterangan']."</td>";
                        echo "<td>".$row['nama_karyawan']."</td>";
                        echo "<td align='center'>";
                        echo "<a href='perhitunganDetail.php?id=".$row['id_perhitungan']."' class='btn btn-xs btn-primary'><i class='fas fa-info'></i> Detail</a>&nbsp;&nbsp;";
                        echo "<a href='perhitunganHapus.php?id=".$row['id_perhitungan']."' onclick='return confirm(\"Hapus perhitungan ".$row['keterangan']." ?\")' class='btn btn-xs btn-danger'><i class='fas fa-trash-alt'></i> Hapus</a>&nbsp;&nbsp;";
                        echo "</td>";
                        echo "</tr>";
                        $no++;
                      }
                    }
                  ?>
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
        </div>
      </div>

    </div>
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<?php
  include_once "_template_bawah.php";
?>
