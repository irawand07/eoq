<?php
  include_once "_template_atas.php";
?>

<?php
  $id = $_GET['id'];

  $dataPerhitungan = query("SELECT * FROM perhitungan WHERE id_perhitungan = '$id' ")[0];
  if(empty($dataPerhitungan)){
    echo "<script>document.location.href = 'perhitungan.php';</script>";
    die();
  }else{
    $conn->begin_transaction();

    $sql = " DELETE FROM `detail_perhitungan`
             WHERE
              `id_perhitungan` = '$id'
           ";
    $delDetail = mysqli_query($conn, $sql);

    $sql = " DELETE FROM `perhitungan`
             WHERE
              `id_perhitungan` = '$id'
           ";
    $delPerhitungan = mysqli_query($conn, $sql);

    $result = $delDetail && $delPerhitungan;
    if($result === true){
      $conn->commit();
      $_SESSION['sukses'] = 'Data perhitungan <b>'.$dataPerhitungan['keterangan'].'</b> berhasil dihapus';
    }else{
      $conn->rollback();
    }
    echo "<script>document.location.href = 'perhitungan.php';</script>";
    die();
  }

?>

<?php
  include_once "_template_bawah.php";
?>
