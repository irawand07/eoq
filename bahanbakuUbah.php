<?php
  include_once "_template_atas.php";
?>

<?php
  if(isset($_POST['id']))
    $id = $_POST['id'];
  else
    $id = $_GET['id'];

  $dataBahanbaku = query("SELECT * FROM bahan_baku WHERE kd_bahanbaku = '$id' ")[0];
  if(empty($dataBahanbaku)){
    echo "<script>document.location.href = 'bahanbaku.php';</script>";
    die();
  }

  $err = '';
  if(isset($_POST['simpan'])){
    $nama    = trim($_POST['nama']);
    $telepon = trim($_POST['telepon']);
    $alamat  = trim($_POST['alamat']);
    $satuan  = trim($_POST['satuan']);

    if($nama == ''){
      $err = 'Kode bahan baku dan nama bahan baku wajib diisi';
    }else{
      $sql = " UPDATE `bahan_baku` SET
                 `nama_bahanbaku` = '$nama',
                 `satuan` = '$satuan'
                WHERE
                `kd_bahanbaku` = '$id'
             ";
      $result = mysqli_query($conn, $sql);
      if($result === true){
        $_SESSION['sukses'] = 'Data berhasil disimpan';
        echo "<script>document.location.href = 'bahanbaku.php';</script>";
        die();
      }else{
        $err = 'Gagal menyimpan data';
      }
    }
  }
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Bahan baku</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
            <li class="breadcrumb-item active">Bahan baku</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">

      <div class="row">
        <div class="col-6 col-md-6  col-sm-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Ubah Bahan baku</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <?php if(!empty($err)) { ?>
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                  <?= $err ?>
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
              <?php } ?>

              <form method="POST">
                <div class="form-group">
                  <label for="exampleInputEmail1">Kode Bahan baku</label>
                  <input disabled type="text" name="kode" class="form-control" placeholder="Kode bahan baku" value="<?= $dataBahanbaku['kd_bahanbaku'] ?>">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Nama Bahan baku</label>
                  <input type="text" name="nama" class="form-control" placeholder="Nama Bahan baku" value="<?= $dataBahanbaku['nama_bahanbaku'] ?>">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Satuan</label>
                  <select name="satuan" class="form-control">
                    <option <?php if($dataBahanbaku['satuan'] == 'Pcs') echo 'selected'; ?> value="Pcs">Pcs</option>
                    <option <?php if($dataBahanbaku['satuan'] == 'Kg') echo 'selected'; ?> value="Kg">Kg</option>
                  </select>
                </div>
                <input type="hidden" name="id" value="<?= $id ?>">
                <button type="submit" name="simpan" class="btn btn-primary">Simpan</button>
                <a href="bahanbaku.php" class="btn btn-default">Batal</a>
              </form>
            </div>
            <!-- /.card-body -->
          </div>
        </div>
      </div>

    </div>
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<?php
  include_once "_template_bawah.php";
?>
