<?php
  include_once "_template_atas.php";
?>

<?php
  $sukses = '';
  if(isset($_SESSION['sukses'])){
    $sukses = $_SESSION['sukses'];
    unset($_SESSION['sukses']);
  }

  $dataBahanbaku = $conn->query("SELECT * FROM bahan_baku ORDER BY kd_bahanbaku ");
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Laporan Bahan baku</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
            <li class="breadcrumb-item ">Laporan</li>
            <li class="breadcrumb-item active">Bahan baku</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">

      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Daftar Bahan baku</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">

              <?php if(!empty($sukses)) { ?>
                <div class="alert alert-info alert-dismissible fade show" role="alert">
                  <?= $sukses ?>
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
              <?php } ?>

              <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th width="50px">No</th>
                  <th>Kode Bahan baku</th>
                  <th>Nama Bahan baku</th>
                  <th>Satuan</th>
                  <th>Harga Satuan (Rp)</th>
                  <th>Stok</th>
                </tr>
                </thead>
                <tbody>
                  <?php
                    if(!empty($dataBahanbaku)){
                      $no = 1;
                      foreach($dataBahanbaku as $row){
                        echo "<tr>";
                        echo "<td>".$no."</td>";
                        echo "<td>".$row['kd_bahanbaku']."</td>";
                        echo "<td>".$row['nama_bahanbaku']."</td>";
                        echo "<td>".$row['satuan']."</td>";
                        if($row['harga_satuan'] == '')
                          echo "<td></td>";
                        else
                          echo "<td align='right'>".number_format($row['harga_satuan'],2)."</td>";
                        echo "<td align='center'>".$row['stok_akhir']."</td>";
                        echo "</tr>";
                        $no++;
                      }
                    }
                  ?>
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
        </div>
      </div>

    </div>
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<?php
  include_once "_template_bawah.php";
?>
