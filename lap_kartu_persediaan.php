<?php
  include_once "_template_atas.php";
?>

<?php
$idBahanbaku = '';

if(isset($_GET['cari'])){
  $idBahanbaku = $_GET['bahanbaku'];

  $dataPersediaan = $conn->query("
    SELECT
      DATE_FORMAT(tanggal,'%d-%m-%Y') AS tgl,
      IF(detail_pembelian.`id_detbeli` IS NOT NULL, 'Pembelian','Pemakaian') AS keterangan,
      detail_pembelian.`qty` AS masuk_qty,
      detail_pembelian.`harga_beli` AS masuk_harga,
      detail_pembelian.`subtotal` AS masuk_total,
      detail_pemakaian.`qty` AS keluar_qty,
      detail_pemakaian.`harga` AS keluar_harga,
      detail_pemakaian.`subtotal` AS keluar_total,
      persediaan.`qty` AS saldo_qty,
      persediaan.`harga` AS saldo_harga,
      persediaan.`total` AS saldo_total
    FROM persediaan
    LEFT JOIN detail_pembelian ON detail_pembelian.`id_detbeli` = persediaan.`id_detbeli`
    LEFT JOIN detail_pemakaian ON detail_pemakaian.`id_detpemakaian` = persediaan.`id_detpemakaian`
    WHERE persediaan.`kd_bahanbaku` = '$idBahanbaku'
  ");
}
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Laporan Kartu Persediaan</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
            <li class="breadcrumb-item ">Laporan</li>
            <li class="breadcrumb-item active">Kartu Persediaan</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">

      <div class="row">
        <div class="col-6">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Pencarian</h3>
            </div>
            <div class="card-body">
              <form method="get">
                <div class="form-group">
                  <label for="exampleInputEmail1">Bahan baku</label>
                  <select name="bahanbaku" class="form-control">
                    <option value=""></option>
                    <?php
                      $dataBahanbaku = $conn->query("
                        SELECT * FROM bahan_baku ORDER BY nama_bahanbaku
                      ");

                      foreach($dataBahanbaku as $row){
                        if($row['kd_bahanbaku'] == $idBahanbaku){
                          echo "<option value='".$row['kd_bahanbaku']."' selected>".$row['nama_bahanbaku']." (".$row['kd_bahanbaku'].")</optio>";
                        }else{
                          echo "<option value='".$row['kd_bahanbaku']."'>".$row['nama_bahanbaku']." (".$row['kd_bahanbaku'].")</optio>";
                        }
                      }
                    ?>
                  </select>
                </div>
                <button type="submit" name="cari" class="btn btn-primary">Cari</button>
              </form>
            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Laporan Kartu Persediaan Per Tanggal <?= date('d-m-Y') ?> </h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">

              <table class="table table-bordered">
                <thead>
                <tr>
                  <th width="150px" rowspan="2" style="text-align:center">Tanggal</th>
                  <th rowspan="2" style="text-align:center">Keterangan</th>
                  <th colspan="3" style="text-align:center">Masuk</th>
                  <th colspan="3" style="text-align:center">Keluar</th>
                  <th colspan="3" style="text-align:center">Saldo</th>
                </tr>
                <tr>
                  <th style="text-align:center">Unit</th>
                  <th style="text-align:center">Harga</th>
                  <th style="text-align:center">Jumlah</th>
                  <th style="text-align:center">Unit</th>
                  <th style="text-align:center">Harga</th>
                  <th style="text-align:center">Jumlah</th>
                  <th style="text-align:center">Unit</th>
                  <th style="text-align:center">Harga</th>
                  <th style="text-align:center">Jumlah</th>
                </tr>
                </thead>
                <tbody>
                  <?php
                    if(!empty($dataPersediaan)){
                      $no = 1;
                      foreach($dataPersediaan as $row){
                        echo "<tr>";
                        echo "<td>".$row['tgl']."</td>";
                        echo "<td>".$row['keterangan']."</td>";
                        if($row['masuk_qty'] == ''){
                          echo "<td></td><td></td><td></td>";
                        }else{
                          echo "<td align='right'>".$row['masuk_qty']."</td>";
                          echo "<td align='right'>".number_format($row['masuk_harga'],2,',','.')."</td>";
                          echo "<td align='right'>".number_format($row['masuk_total'],2,',','.')."</td>";
                        }

                        if($row['keluar_qty'] == ''){
                          echo "<td></td><td></td><td></td>";
                        }else{
                          echo "<td align='right'>".$row['keluar_qty']."</td>";
                          echo "<td align='right'>".number_format($row['keluar_harga'],2,',','.')."</td>";
                          echo "<td align='right'>".number_format($row['keluar_total'],2,',','.')."</td>";
                        }

                        echo "<td align='right'>".$row['saldo_qty']."</td>";
                        echo "<td align='right'>".number_format($row['saldo_harga'],2,',','.')."</td>";
                        echo "<td align='right'>".number_format($row['saldo_total'],2,',','.')."</td>";

                        echo "</tr>";
                        $no++;
                      }
                    }
                  ?>
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
        </div>
      </div>

    </div>
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<?php
  include_once "_template_bawah.php";
?>
